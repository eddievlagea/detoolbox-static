App.Router.map(function () {
  this.route("about", { path: "/about" });
});


App.IndexRoute = Ember.Route.extend({
  setupController: function(controller, user) {
    controller.set('model', user);
  }
});