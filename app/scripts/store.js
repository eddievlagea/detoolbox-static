App.Store = DS.Store.extend({
  adapter: DS.CouchDBAdapter.create({
    host:'http://127.0.0.1:5984',
    db: 'detoolkit',
    designDoc: 'de-views'
  }),
  revision: 13
});