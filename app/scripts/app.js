var App = window.App = Ember.Application.create({
  // LOG_TRANSITIONS: true,
  // LOG_TRANSITIONS_INTERNAL: true
});

/* Order and include as you please. */
require('scripts/routes/*');
require('scripts/controllers/*');
require('scripts/models/*');
require('scripts/views/*');
require('scripts/router');
require('scripts/store');

Ember.GoogleAnalytics = Ember.Mixin.create({

  trackPageView: function() {
    if(!Ember.isNone(ga)) {
      Ember.run.next(function() {
        var loc = window.location,
            page = loc.hash ? loc.hash.substring(1) : loc.pathname + loc.search;
        ga('send', 'pageview', page);
      });
    }
  }.observes('currentPath')
});


App.Controller = Em.ArrayController.extend(Ember.GoogleAnalytics, {
  // Scroll to login form
  jumpToLogin : function(){
    this.transitionToRoute('index');
    setTimeout(function() {
      $('html, body').animate({ scrollTop: $("#login").offset().top - 100}, 100);

      $('input[type="email"]').focus();
    }, 400);
  },

  jumpToContact : function(){
    this.transitionToRoute('about');
    setTimeout(function() {
      $('html, body').animate({ scrollTop: $("#contact-us").offset().top }, 100);
    }, 0);
  },

  jumpToAbout : function(){
    this.transitionToRoute('about');
    setTimeout(function() { window.scrollTo(0 ,0);}, 0);
  },
});

App.IndexController = Ember.ArrayController.extend({
 saveUser: function () {
    this.setProperties({
      success: false,
      failure: false
    });

    // Get the user data
    // var first_name = this.get('first_name'),
    var email = this.get('email');
    console.log("email", email);

    if (email === undefined || email.trim() === "") {
      this.set("success", false);
      this.set("failure", true);
    }

    else {
      this.set("success", true);
    }

    // Create the new Todo model
    App.User.createRecord({
      // first_name: first_name,
      ember_type: 'App.User',
      email: email
    });

    // Clear text fields
    // this.set('first_name', '');
    this.set('email', '');

    // Save the new model
    this.get('store').commit();
  }
});
